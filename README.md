ug-clean-backup-directory is a tool that helps to delete, conveniently, files created by some backup systems


# Install it 
## Requirements
* python3
* python3 yaml

## Debian
```bash
apt install python3-yaml
```
## The script
```bash
cp ug-clean-backup-directory /usr/local/bin/ug-clean-backup-directory
chmod 700 /usr/local/bin/ug-clean-backup-directory
```

# Configuration file
copy [ug-clean-backup-directory.conf.sample](ug-clean-backup-directory.conf.sample) to ```/etc/ug-clean-backup-directory.conf``` and edit it.
```yaml
gitlab-backup:
  engine: clean1
  directory: /gitlab/backups/
  regex: \S+_gitlab_backup.tar
  nb_files_max: 30
```
This will go in ```/gitlab/backups```, list all the files with the regex ```\S+_gitlab_backup.tar``` in it and keeps only the last 30 files.

# Test it
Simply launch
```bash
ug-clean-backup-directory -n
  # Do Config (gitlab-backup) with engine (clean1).
  # Delete file (/gitlab/backups/1489539654_2017_03_15_gitlab_backup.tar).
  # Delete file (/gitlab/backups/1489626053_2017_03_16_gitlab_backup.tar).
```
and modify the configuration file at your willing.

# Lanuch it
```bash
ug-clean-backup-directory
  # Do Config (gitlab-backup) with engine (clean1).
  # Delete file (/gitlab/backups/1489539654_2017_03_15_gitlab_backup.tar).
  # Delete file (/gitlab/backups/1489626053_2017_03_16_gitlab_backup.tar).
```

# Production
Use the ug-clean-backup-directory.cron:
```bash
cp ug-clean-backup-directory.cron /etc/cron.daily/ug-clean-backup-directory
```

# Logs
Log files are stored in ```/var/log/ug-clean-backup-directory.log```
